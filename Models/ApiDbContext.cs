using Microsoft.EntityFrameworkCore;
using slnControleBicicleta.Models;

namespace ControleBicicleta.Models
{
  public class ApiDbContext : DbContext
  {
    public ApiDbContext(DbContextOptions<ApiDbContext> options)
        : base(options)
    { }

    public DbSet<Empresa> Empresa{ get; set; }
    public DbSet<Bicicleta> Bicicleta { get; set; }
    public DbSet<Cliente> Cliente { get; set; }
    
  }
}
