    using System;
using System.Collections.Generic;

namespace slnControleBicicleta.Models
{
   public class Bicicleta{
          
          public Bicicleta(){

          }

          public int Id { get; set;}
         
          public string Marca { get; set;}

          public string Cor { get; set;}

           public int CnpjId {get; set;}

           public Empresa Empresa {get; set;}

           public ICollection<Cliente> Clientes {get; set;}

        
      }

}