using System;
using System.Collections.Generic;

namespace slnControleBicicleta.Models
{
   public class Empresa{
          
          public Empresa(){

          }

          public int  CnpjId { get; set;}
         
          public string Nome { get; set;}

          public int Cel_1 { get; set;}

          public int Cel_2 { get; set;}

          public string Email { get; set;}

          public int Num {get; set;}

          public string Cidade {get; set;}

          public string Bairro {get; set;}

          public string Rua {get; set;}
          
          public ICollection<Bicicleta> Bicicletas {get; set;}
    
        
          


        
      }

}