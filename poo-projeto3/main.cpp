// aula dia 04/09
#include <cstdlib>
#include <iostream>

using namespace std;

class aluno {
private:
    int matricula; //número da matrícula 
    string nome; //nome 
    char sexo; //sexo (M/F/I/N)
    float mensalidade; //mensalidade 
    bool status; //status (true = matriculado, false = não matriculado)

public:

    void setMatricula(int valor) {
        matricula = valor;
    }
    
    int getMatricula(){
        return matricula;
    }

    void setNome() {

    }

    void setSexo() {

    }

    void setMensalidade() {

    }

    void setStatus() {

    }
};

int main() {

    aluno aluno1;
    
    aluno1.setMatricula(1000);
    
    cout << aluno1.getMatricula();
    
    return 0;
}