// aula dia 04/09
#include <cstdlib>
#include <iostream>

using namespace std;

class aluno {
public:
    int matricula; //número da matrícula 
    string nome; //nome 
    char sexo; //sexo (M/F/I/N)
    float mensalidade; //mensalidade 
    bool status; //status (true = matriculado, false = não matriculado)
};

int main() {
    //instanciar classe, criando objeto: 
    aluno aluno1;
    aluno aluno2;

    //preenchendo o objeto aluno: 
    aluno1.matricula = 1000;
    aluno1.nome = "André";
    aluno1.sexo = 'M';
    aluno1.mensalidade = 1250;
    aluno1.status = true;

    cout << "Dados cadastrados do aluno1: " << endl;
    cout << "Matrícula: " << aluno1.matricula << endl;
    cout << "Nome: " << aluno1.nome << endl;
    cout << "Sexo: " << aluno1.sexo << endl;
    cout << "Mensalidade: " << aluno1.mensalidade << endl;

    if (aluno1.status == true) {
        cout << "Matriculado\n";
    } else {
        cout << "Não matriculado\n";
    }

    cout << "\nEntre com os dados do aluno 2: " << endl;
    cout << "Matrícula: " << endl;
    cin >> aluno2.matricula;
    cout << "Nome: " << endl;
    cin >> aluno2.nome;
    cout << "Sexo: " << endl;
    cin >> aluno2.sexo;
    cout << "Mensalidade: " << endl;
    cin >> aluno2.mensalidade;
    cout << "Status: " << endl;
    cin >> aluno2.status;

    cout << "\nDados cadastrados do aluno2: " << endl;
    cout << "Matrícula: " << aluno2.matricula << endl;
    cout << "Nome: " << aluno2.nome << endl;
    cout << "Sexo: " << aluno2.sexo << endl;
    cout << "Mensalidade: " << aluno2.mensalidade << endl;

    if (aluno2.status == true) {
        cout << "Matriculado\n";
    } else {
        cout << "Não matriculado\n";
    }
    return 0;
}
